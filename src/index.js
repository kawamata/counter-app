import React from 'react';
import ReactDOM from 'react-dom';
import CounterContext from './contexts/counter';
import Counter from './components/counter';

// class + constructorは、これまでの (var ○○○ =) function(){} として
// newを使って新たなオブジェクトとして関数を呼び出す場合と同じもの（クラスの様な使い方してた）
// なので以下の場合は基本　const ○○○ = new App() としてオブジェクト生成する
// superは、extendsで他クラスを継承する時に、継承元のメソッドを呼べる様にする機能
// superに引数を渡せば、継承元クラスに引数が渡る
// https://www.yunabe.jp/docs/javascript_class_es6.html
class App extends React.Component {
  constructor(props) {
    super(props)
    this.increment = this.increment.bind(this)
    this.decrement = this.decrement.bind(this)

    this.state = {
      count: 0,
      increment: this.increment,
      decrement: this.decrement
    }
  }
  increment(){
    this.setState({count: this.state.count + 1})
  }
  decrement(){
    this.setState({count: this.state.count - 1})
  }
  render(){
    return (
      // ReactのProviderの使い方（以下のようにcomponentを作成）
      // これは、他のcpmponentをラップできる
      <CounterContext.Provider value={this.state}>
        <Counter />
      </CounterContext.Provider>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
