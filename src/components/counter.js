import React from 'react';
import CounterContext from '../contexts/counter';

const Counter = () =>(
  // 以下componentは、中に関数を書くことがルール
  // （何故なら、ここで<CounterContext.Provider>で設定したvalueの値を受け取れる
  <CounterContext.Consumer>
   {
     ({count, increment, decrement}) =>{
       return (
         <React.Fragment>
          <div>count: {count}</div>
          <button onClick={increment}>+1</button>
          <button onClick={decrement}>-1</button>
         </React.Fragment>
       )
     }
   }
  </CounterContext.Consumer>
)

export default Counter
